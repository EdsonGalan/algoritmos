'use strict';
function procesar(){
  // Obtiene los arreglos
  let array1 = document.getElementById('conjunto1').value.split(',');
  let array2 = document.getElementById('conjunto2').value.split(',');
  let array3 = [];    // Guardará los números que se repitan en ambos arreglos
  let i=0; let j=0;
  // Comienza a recorrer los dos array
  while(array1[i] && array2[j]){
    if( parseInt(array1[i]) == parseInt(array2[j]) ){
      array3.push(array1[i]);
      i++;
      j++;
    }else if( parseInt(array1[i]) < parseInt(array2[j]) ){
      i++;
    }else{
      j++;
    }
  }
  let str_resultado = "";
  for( let n of array3){
    str_resultado += n + ',';
  }
  document.getElementById('repetidos').value= str_resultado;
}

procesar();
